# Bài tập git-workflow
## Lý thuyết
    - tài liệu: https://www.makeareadme.com/
### kiến thức nắm được
    - git là gì, tại sao phải sử dụng git
    - cách làm việc nhóm. Chịu trách nhiệm trên từng commit
    - description mô tả ngắn những gì sửa đổi trong commit
    - body mô tả chi tiết
    - thông tin mở rộng id, request, issue
    - 1 số type phổ biến:
        - feat: thêm 1 feature
        - fix: fix bug
        - docs: sửa document
        - chore: sửa đổi nhỏ không liên quan đến code
        - perf: cải tiến hiệu năng
        - vendor: nâng cấp version
## thực hành
### Basic
    - tạo 1 repo trên gitlap
    - chia thành main và deverlop
    - up lên deverlop
    - tạo commit theo mẫu
    - merge deverlop vào main
    - viết README.md
### Advance
    - xử lý conflict
    - revert commit: git revert "commit cần revert"
    - reset commit: git reset --hard "commit cần reset"
    - cherry-pick commit: 
## workflow